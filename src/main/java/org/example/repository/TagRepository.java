package org.example.repository;

import lombok.RequiredArgsConstructor;
import org.example.model.Tag;
import org.example.repository.mapper.TagRowMapper;
import org.springframework.dao.DataAccessException;
import org.springframework.dao.EmptyResultDataAccessException;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.stereotype.Repository;

import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

@Repository
@RequiredArgsConstructor
public class TagRepository implements CrdRepository<Tag> {
    private final JdbcTemplate jdbcTemplate;
    private final TagRowMapper tagRowMapper;

    @Override
    public List<Tag> findAll() {
        String sql = "SELECT tag_id, tag_name FROM tags";

        try {
            return jdbcTemplate.query(sql, tagRowMapper);
        } catch (Exception e) {
            return new ArrayList<Tag>();
        }
    }

    @Override
    public void add(Tag tag) {
        String sql = "INSERT INTO public.tags (tag_name) VALUES (?)";

        int insert = jdbcTemplate.update(sql, tag.getTagName());

        System.out.println("insert value: " + insert);

        if (insert == 1) { // if 1 line is returned, 1 element was created
            System.out.println("New tag created: " + tag.getTagId());
        }
    }

    @Override
    public boolean existsById(int id) {
        String sql = "SELECT tag_id, tag_name FROM tags WHERE tag_id = ? ";
        return jdbcTemplate.queryForObject(sql, new Object[]{id}, tagRowMapper) != null? true : false;
    }

    public boolean existsByName(String name) throws SQLException {
        String sql = "SELECT tag_id, tag_name FROM tags WHERE tag_name = ? ";

        try {
            return jdbcTemplate.queryForObject(sql, new Object[]{name}, tagRowMapper) != null? true : false;
        } catch (EmptyResultDataAccessException e) {
            return false;
        }
    }

    @Override
    public Optional<Tag> find(int id) {
        String sql = "SELECT tag_id, tag_name FROM tags WHERE tag_id = ? ";
        Tag tag = null;
        try {
            tag = jdbcTemplate.queryForObject(sql, new Object[]{id}, tagRowMapper);
        } catch (DataAccessException e) {
            System.out.println("Tag not found: " + e);
        }
        return Optional.ofNullable(tag);
    }

    @Override
    public void delete(int id) {
        String sql = "DELETE FROM public.tags WHERE tag_id = ?";
        jdbcTemplate.update(sql, id);
    }
}

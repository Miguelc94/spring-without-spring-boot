package org.example.repository;

import java.sql.SQLException;
import java.util.List;
import java.util.Optional;

public interface CrdRepository<T> {
    List<T> findAll() throws SQLException;
    void add(T t);
    boolean existsById(int id) throws SQLException;
    Optional<T> find(int id);
    void delete(int id);
}

package org.example.repository.mapper;

import org.example.model.GiftCertificate;
import org.example.model.Tag;
import org.springframework.jdbc.core.RowMapper;
import org.springframework.stereotype.Component;

import java.sql.ResultSet;
import java.sql.SQLException;

@Component
public class GiftCertificateRowMapper implements RowMapper<GiftCertificate> {
    @Override
    public GiftCertificate mapRow(ResultSet rs, int rowNum) throws SQLException {
        return GiftCertificate.builder()
                .giftId(rs.getInt("gift_id"))
                .giftName(rs.getString("gift_name"))
                .description(rs.getString("description"))
                .price(rs.getFloat("price"))
                .duration(rs.getLong("duration"))
                .createDate(rs.getString("create_date"))
                .lastUpdateDate(rs.getString("last_update_date"))
                .build();
    }

//    private Tag mapTag(ResultSet rs) {
//
//    }
}

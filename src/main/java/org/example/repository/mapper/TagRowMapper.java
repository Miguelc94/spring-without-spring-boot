package org.example.repository.mapper;

import org.example.model.Tag;
import org.springframework.jdbc.core.RowMapper;
import org.springframework.stereotype.Component;

import java.sql.ResultSet;
import java.sql.SQLException;

@Component
public class TagRowMapper implements RowMapper<Tag> {

    @Override
    public Tag mapRow(ResultSet rs, int rowNum) throws SQLException {
        return Tag.builder()
                .tagId(rs.getInt("tag_id"))
                .tagName(rs.getString("tag_name"))
                .build();
    }
}

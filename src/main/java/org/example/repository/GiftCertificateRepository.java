package org.example.repository;

import lombok.RequiredArgsConstructor;
import org.example.model.GiftCertificate;
import org.example.repository.mapper.GiftCertificateRowMapper;
import org.springframework.dao.DataAccessException;
import org.springframework.dao.EmptyResultDataAccessException;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.stereotype.Repository;

import java.sql.SQLException;
import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;
import java.util.*;

@Repository
@RequiredArgsConstructor
public class GiftCertificateRepository implements CrudRepository<GiftCertificate> {
    private final JdbcTemplate jdbcTemplate;
    private final GiftCertificateRowMapper giftRowMapper;


    //@Override
    public List<GiftCertificate> findAll(
            //String[] params
    ) {
        StringBuilder sql = new StringBuilder("SELECT c.gift_id, c.gift_name, c.description, c.price, c.duration, c.create_date, c.last_update_date FROM public.gift_certificates c ");


//        if (!Arrays.stream(params).allMatch(Objects::isNull)) {
//            if (params[1] != null) {
//                sql.append(" WHERE (c.gift_name LIKE '%" + params[1] + "%' OR c.description LIKE '%" + params[1] + "%' )");
//            }
//
//            if (params[2] != null)
//                sql.append("GROUP BY c.gift_id ORDER BY c.gift_name " + params[2]);
//
//        } else
        sql.append("GROUP BY c.gift_id ORDER BY c.gift_id");

        try {
            return jdbcTemplate.query(sql.toString(), giftRowMapper);
        } catch (Exception e) {
            return new ArrayList<GiftCertificate>();
        }

    }

    @Override
    public void add(GiftCertificate certificate) {
        String sql = "INSERT INTO public.gift_certificates " +
                "(gift_name, description, price, duration, create_date) " +
                "VALUES (?, ?, ?, ?, ?)";

        DateTimeFormatter dtf = DateTimeFormatter.ofPattern("uuuu-MM-dd'T'HH:mm:ss.SSS");
        LocalDateTime now = LocalDateTime.now();
        int insert = jdbcTemplate.update(sql, certificate.getGiftName()
                , certificate.getDescription(), certificate.getPrice(), certificate.getDuration()
                , dtf.format(now));

        if (insert == 1) { // if 1 line is returned, 1 element was created
            System.out.println("New certificate created: " + certificate.getGiftId());
        }
    }

    @Override
    public boolean existsById(int id) {
        String sql = "SELECT * FROM public.gift_certificates WHERE gift_id = ?";
        try {
            //why is this always returning false?
            return jdbcTemplate.queryForObject(sql, new Object[]{id}, giftRowMapper) != null;
        } catch (EmptyResultDataAccessException e) {
            return false;
        }
    }

    @Override
    public Optional<GiftCertificate> find(int id) {
        String sql = "SELECT * FROM public.gift_certificates WHERE gift_id = ?";
        GiftCertificate gift = null;
        try {
            gift = jdbcTemplate.queryForObject(sql, new Object[]{id}, giftRowMapper);
        } catch (DataAccessException e) {
            System.out.println("Gift not found: " + e);
        }
        return Optional.ofNullable(gift);
    }

    @Override
    public void update(GiftCertificate gift, int id) {
        System.out.println("gift to update: " + gift);
        String sql = "UPDATE public.gift_certificates SET gift_name = ? , description = ? , price = ? , duration = ? , last_update_date = ?  WHERE gift_id = ? ";

        DateTimeFormatter dtf = DateTimeFormatter.ofPattern("uuuu-MM-dd'T'HH:mm:ss.SSS");
        LocalDateTime now = LocalDateTime.now();
        int insert = jdbcTemplate.update(sql, gift.getGiftName(), gift.getDescription(), gift.getPrice(), gift.getDuration(), dtf.format(now), id);

        if (insert == 1) { // if 1 line is returned, 1 element was updated
            System.out.println("Gift updated: " + id);
        }
    }

    @Override
    public void delete(int id) {
        String sql = "DELETE FROM public.gift_certificates WHERE gift_id = ?";
        jdbcTemplate.update(sql, id);
    }


}

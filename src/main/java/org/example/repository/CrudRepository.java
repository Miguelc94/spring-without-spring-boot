package org.example.repository;

import java.sql.SQLException;
import java.util.Optional;

public interface CrudRepository<T> {
    void add(T t);
    boolean existsById(int id) throws SQLException;
    Optional<T> find(int id);
    void update(T t, int id);
    void delete(int id);

}

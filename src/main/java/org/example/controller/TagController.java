package org.example.controller;

import lombok.RequiredArgsConstructor;
import org.example.exceptions.EntityNotFoundException;
import org.example.model.Tag;
import org.example.service.TagService;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.*;

import java.sql.SQLException;
import java.util.List;

@RestController
@RequestMapping("/api/v1/tags")
@RequiredArgsConstructor
public class TagController {
    private final TagService service;


    // URL: http://localhost:8080/api/v1/tags?certificateId=1
    // List<Tag> getByCertificateId(@RequestParam(required = true) String certificateId)

    @GetMapping
    public List<Tag> findAll() throws SQLException {
        return service.list();
    }


    @GetMapping("/{id}")
    public Tag find(@PathVariable int id) throws SQLException {
        return service.get(id).orElseThrow(() -> new EntityNotFoundException(""));
    }

    @PostMapping("") //if we want another url for the creation of courses
    @ResponseStatus(HttpStatus.CREATED)
    public void add(@RequestBody Tag tag) throws SQLException {
        service.create(tag);
    }

    @DeleteMapping("/{id}")
    @ResponseStatus(HttpStatus.NO_CONTENT)
    public void delete(@PathVariable int id) {
        service.delete(id);
    }
}

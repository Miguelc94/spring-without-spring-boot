package org.example.controller.advice;

import org.example.exceptions.EntityNotFoundException;
import org.example.model.ErrorResponse;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.ControllerAdvice;
import org.springframework.web.bind.annotation.ExceptionHandler;

import static org.springframework.http.HttpStatus.NOT_FOUND;


@ControllerAdvice
public class ExceptionHandlingControllerAdvice {

    @ExceptionHandler(EntityNotFoundException.class)
    public ResponseEntity<ErrorResponse> handleEntityNotFoundException(EntityNotFoundException e) {
        // TODO: how to send the exception message to the response

        ErrorResponse errorResponse = ErrorResponse.builder()
                .errorCode("ERR-001") // ERR-001 is
                .message("The requested resource was not found")
                .build();

        return ResponseEntity.status(NOT_FOUND).body(errorResponse);
    }
}

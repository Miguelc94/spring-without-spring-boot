package org.example.controller;

import lombok.RequiredArgsConstructor;
import org.example.model.GiftCertificate;
import org.example.model.request.CreateGiftCertificateRequest;
import org.example.service.GiftCertificateService;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.server.ResponseStatusException;

import java.sql.SQLException;
import java.util.List;

@RestController
@RequestMapping("/api/v1/certificates")
@RequiredArgsConstructor
public class GiftCertificateController {

    private final GiftCertificateService service;


    @GetMapping
    public List<GiftCertificate> findAll(@RequestParam(value = "tagName", required = false) String tagName,
                                         @RequestParam(value = "searchBy", required = false) String searchBy,
                                         @RequestParam(value = "sort", required = false) String sort
        ) throws SQLException {
        return service.list(new String[]{tagName,searchBy,sort});
    }

    @GetMapping("/{id}")
    public GiftCertificate find(@PathVariable int id) throws SQLException {
        return service.get(id).orElseThrow(() -> new ResponseStatusException(HttpStatus.NOT_FOUND,"Content not found"));
    }

    @PostMapping("") //if we want another url for the creation of courses
    @ResponseStatus(HttpStatus.CREATED)
    public void add(@RequestBody CreateGiftCertificateRequest gift) throws SQLException {
        service.create(gift);
    }

    @PutMapping("/{id}")
    @ResponseStatus(HttpStatus.NO_CONTENT)
    public void update(@RequestBody CreateGiftCertificateRequest gift, @PathVariable int id) throws SQLException {
        System.out.println(gift);
        service.update(gift, id);
    }


    @DeleteMapping("/{id}")
    @ResponseStatus(HttpStatus.NO_CONTENT)
    public void delete(@PathVariable int id) {
        service.delete(id);
    }
}

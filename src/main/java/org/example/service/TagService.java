package org.example.service;

import lombok.RequiredArgsConstructor;
import org.example.model.Tag;
import org.example.repository.TagRepository;
import org.springframework.stereotype.Service;

import java.sql.SQLException;
import java.util.List;
import java.util.Optional;

@Service
@RequiredArgsConstructor
public class TagService implements CrdService<Tag> {
    private final TagRepository repository;

    @Override
    public List<Tag> list() {
        return repository.findAll();
    }

    @Override
    public void create(Tag tag) {
        try {
            if (!existsByName(tag.getTagName()))
                repository.add(tag);
            else
                System.out.println("Tag already exists");
        } catch (SQLException e) {
            e.printStackTrace();
        }
    }

    @Override
    public Optional<Tag> get(int id) {
        return repository.find(id);
    }

    @Override
    public boolean existsById(int id) throws SQLException {
        return repository.existsById(id);
    }

    public boolean existsByName(String name) throws SQLException {
        return repository.existsByName(name);
    }

    @Override
    public void delete(int id) { //it should not be able to delete tags with certificates
        repository.delete(id);
    }
}

package org.example.service;

import lombok.RequiredArgsConstructor;
import org.example.exceptions.EntityNotFoundException;
import org.example.model.GiftCertificate;
import org.example.model.Tag;
import org.example.model.request.CreateGiftCertificateRequest;
import org.example.repository.GiftCertificateRepository;
import org.springframework.stereotype.Service;

import java.sql.SQLException;
import java.util.List;
import java.util.Optional;

@Service
@RequiredArgsConstructor
public class GiftCertificateService {

    private final GiftCertificateRepository repository;
    private final TagService tagService;

    public List<GiftCertificate> list(String[] params) throws SQLException {
        return repository.findAll();
    }

    public void create(CreateGiftCertificateRequest createGiftRequest) {
        List<Tag> tags = createGiftRequest.getTags();
        if (tags != null) {
            tags.forEach(tagService::create);
        }

        repository.add(createGiftRequest.getGiftCertificate());
    }

    public Optional<GiftCertificate> get(int id) {
        return repository.find(id);
    }

    public boolean existsById(int id) {
        return repository.existsById(id);
    }

    public void update(CreateGiftCertificateRequest createGiftRequest, int id) {
        if (!existsById(id)) {
            throw new EntityNotFoundException("Gift certificate with id %s not found".formatted(id));
        }

        List<Tag> tags = createGiftRequest.getTags();
        if (tags != null) {
            tags.forEach(tagService::create);
        }
        repository.update(createGiftRequest.getGiftCertificate(), id);
    }

    public void delete(int id) {
        repository.delete(id);
    }
}

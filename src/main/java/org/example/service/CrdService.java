package org.example.service;

import java.sql.SQLException;
import java.util.List;
import java.util.Optional;

public interface CrdService<T> {
    List<T> list() throws SQLException;

    void create(T t);

    Optional<T> get(int id);

    boolean existsById(int id) throws SQLException;

    void delete(int id);
}

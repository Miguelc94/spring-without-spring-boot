package org.example;


import org.example.config.DataSourceConfig;
import org.springframework.context.annotation.AnnotationConfigApplicationContext;
import org.springframework.jdbc.core.JdbcTemplate;


public class Application {

    public static void main(String[] args) {
        AnnotationConfigApplicationContext context = new AnnotationConfigApplicationContext(DataSourceConfig.class);
        JdbcTemplate jdbcTemplate = context.getBean(JdbcTemplate.class);
        /*
        String sql = "SELECT * FROM PUBLIC.COURSES";
        RowMapper<Course> rowMapper = (rs, rowNum) -> {
            Course course = Course.builder()
                    .courseId(rs.getInt("course_id"))
                    .title(rs.getString("title"))
                    .description(rs.getString("description"))
                    .link(rs.getString("link"))
                    .build();
            return course;
        };

        System.out.println(jdbcTemplate.query(sql, rowMapper));
        */
        /*
        Course course = Course.builder()
                .title("google2")
                .description("another google course")
                .link("google.es")
                .build();;
        String sql = "INSERT INTO public.courses (title, description, link) VALUES (\'" + course.getTitle()
                + "\',\'"+ course.getDescription() + "\',\'" + course.getLink() + "\')";
        int insert = jdbcTemplate.update(sql);
        if (insert == 1) {
            System.out.println("New course created: " + course.getTitle());
        }*/

    }
}

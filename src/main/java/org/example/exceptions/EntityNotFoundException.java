package org.example.exceptions;


public class EntityNotFoundException extends RuntimeException {
    public EntityNotFoundException(String formatted) {
        super(formatted);
    }
}

package org.example.model;

import lombok.Builder;
import lombok.Data;
import lombok.extern.jackson.Jacksonized;


@Data
@Builder
@Jacksonized
public class Tag {
    private int tagId;
    private String tagName;
}

package org.example.model.request;

import lombok.Builder;
import lombok.Data;
import lombok.extern.jackson.Jacksonized;
import org.example.model.GiftCertificate;
import org.example.model.Tag;

import java.util.List;

@Data
@Builder
@Jacksonized
public class CreateGiftCertificateRequest {
    private GiftCertificate giftCertificate;
    private List<Tag> tags;
}

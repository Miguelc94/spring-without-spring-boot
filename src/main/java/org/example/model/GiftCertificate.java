package org.example.model;

import lombok.Builder;
import lombok.Data;
import lombok.extern.jackson.Jacksonized;

@Data
@Builder
@Jacksonized
public class GiftCertificate {
    private int giftId;
    private String giftName;
    private String description;
    private float price; //8,2
    private long duration; //in days
    private String createDate;
    private String lastUpdateDate;
}

package model;

import org.example.model.GiftCertificate;
import org.example.repository.GiftCertificateRepository;
import org.example.repository.mapper.GiftCertificateRowMapper;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.Mock;
import org.mockito.junit.jupiter.MockitoExtension;
import org.springframework.dao.EmptyResultDataAccessException;
import org.springframework.jdbc.core.JdbcTemplate;

import java.sql.SQLException;
import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;
import java.util.ArrayList;
import java.util.List;
import java.util.Optional;



import static org.assertj.core.api.Assertions.assertThat;
import static org.mockito.Mockito.*;

@ExtendWith(MockitoExtension.class)
public class GiftCertificateRepositoryTest {
    @Mock
    private JdbcTemplate jdbcTemplate;
    @Mock
    private GiftCertificateRowMapper rowMapper;
    @Mock
    private GiftCertificateRepository repository;

    @BeforeEach
    void setUp() throws Exception {
        repository = new GiftCertificateRepository(jdbcTemplate, rowMapper);
    }

    @Test
    void testGetCert() {
        String expectedSql = "SELECT * FROM public.gift_certificates WHERE gift_id = ?";
        GiftCertificate gift = GiftCertificate.builder().build();
        int id = 1;

        when(jdbcTemplate.queryForObject(expectedSql, new Object[]{id}, rowMapper)).thenReturn(gift); //listens what objects are used and returned

        Optional<GiftCertificate> actual = repository.find(id);

        verify(jdbcTemplate).queryForObject(expectedSql, new Object[]{id}, rowMapper);
        verifyNoMoreInteractions(jdbcTemplate);

        assertThat(actual).contains(gift);
    }

    @Test
    void testCreateCert() {
        String expectedSql = "INSERT INTO public.gift_certificates " +
                "(gift_name, description, price, duration, create_date) " +
                "VALUES (?, ?, ?, ?, ?)";

        DateTimeFormatter dtf = DateTimeFormatter.ofPattern("uuuu-MM-dd'T'HH:mm:ss.SSS");
        LocalDateTime now = LocalDateTime.now();

        GiftCertificate gift = GiftCertificate.builder()
                .giftName("EPAM cert").description("Certificate from EPAM")
                .price(10.0f).duration(25).createDate(dtf.format(now)).build();

        when(jdbcTemplate.update(eq(expectedSql), eq(gift.getGiftName()), eq(gift.getDescription())
                , eq(gift.getPrice()), eq(gift.getDuration()), anyString())).thenReturn(1); //listens what objects are used and returned

        repository.add(gift);

        verify(jdbcTemplate).update(eq(expectedSql), eq(gift.getGiftName()), eq(gift.getDescription())
                , eq(gift.getPrice()), eq(gift.getDuration()), anyString());

        verifyNoMoreInteractions(jdbcTemplate);
    }

    @Test
    void testUpdateCert() {
        String expectedSql = "UPDATE public.gift_certificates SET gift_name = ? , description = ? , price = ? , duration = ? , last_update_date = ?  WHERE gift_id = ? ";

        DateTimeFormatter dtf = DateTimeFormatter.ofPattern("uuuu-MM-dd'T'HH:mm:ss.SSS");
        LocalDateTime now = LocalDateTime.now();

        GiftCertificate gift = GiftCertificate.builder()
                .giftName("EPAM cert").description("Certificate from EPAM")
                .price(10.0f).duration(25).createDate(dtf.format(now)).build();

        when(jdbcTemplate.update(eq(expectedSql), eq(gift.getGiftName()), eq(gift.getDescription())
                , eq(gift.getPrice()), eq(gift.getDuration()), anyString(), anyInt())).thenReturn(1); //listens what objects are used and returned

        repository.update(gift,1);

        verify(jdbcTemplate).update(eq(expectedSql), eq(gift.getGiftName()), eq(gift.getDescription())
                , eq(gift.getPrice()), eq(gift.getDuration()), anyString(), eq(1));

        verifyNoMoreInteractions(jdbcTemplate);
    }

    @Test
    void testDeleteCert() {
        String expectedSql = "DELETE FROM public.gift_certificates WHERE gift_id = ?";

        repository.delete(1);

        verify(jdbcTemplate).update(expectedSql, 1);

        verifyNoMoreInteractions(jdbcTemplate);
    }

    @Test
    void testListCert() throws SQLException {
        String expectedSql = "SELECT c.gift_id, c.gift_name, c.description, c.price, c.duration, c.create_date, c.last_update_date FROM public.gift_certificates c GROUP BY c.gift_id ORDER BY c.gift_id";
        GiftCertificate gift = GiftCertificate.builder().build();
        GiftCertificate gift2 = GiftCertificate.builder().build();
        int id = 1;
        List<GiftCertificate> expected = new ArrayList<>(){};
        expected.add(gift);

        when(jdbcTemplate.query(expectedSql, rowMapper)).thenReturn(expected); //listens what objects are used and returned

        List<GiftCertificate> actual = repository.findAll();

        verify(jdbcTemplate).query(expectedSql, rowMapper);
        verifyNoMoreInteractions(jdbcTemplate);

        assertThat(actual).isEqualTo(expected);
    }

    @Test
    void existsById_whenGiftIsFound_returnsTrue() throws SQLException {
        String expectedSql = "SELECT * FROM public.gift_certificates WHERE gift_id = ?";
        int id = 1;

        when(jdbcTemplate.queryForObject(expectedSql, new Object[]{id},  rowMapper)).thenReturn(GiftCertificate.builder().build());

        boolean exists = repository.existsById(id);

        verify(jdbcTemplate).queryForObject(expectedSql, new Object[]{id}, rowMapper);
        verifyNoMoreInteractions(jdbcTemplate);

        assertThat(exists).isTrue();
    }

    @Test
    void existsById_whenGiftIsNotFound_returnsFalse() {
        String expectedSql = "SELECT * FROM public.gift_certificates WHERE gift_id = ?";
        int id = 99;

        when(jdbcTemplate.queryForObject(expectedSql, new Object[]{id},  rowMapper)).thenReturn(null);

        boolean exists = repository.existsById(id);

        verify(jdbcTemplate).queryForObject(expectedSql, new Object[]{id}, rowMapper);
        verifyNoMoreInteractions(jdbcTemplate);

        assertThat(exists).isFalse();
    }
}

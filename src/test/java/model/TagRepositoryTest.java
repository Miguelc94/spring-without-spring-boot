package model;

import org.example.model.Tag;
import org.example.repository.TagRepository;
import org.example.repository.mapper.TagRowMapper;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.Mock;
import org.mockito.junit.jupiter.MockitoExtension;
import org.springframework.jdbc.core.JdbcTemplate;

import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

import static org.assertj.core.api.Assertions.assertThat;
import static org.mockito.Mockito.*;

@ExtendWith(MockitoExtension.class)
public class TagRepositoryTest {

    @Mock
    private JdbcTemplate jdbcTemplate;

    @Mock
    private TagRowMapper rowMapper;

    @Mock
    private TagRepository subject;

    @BeforeEach
    void setUp() throws Exception {
        subject = new TagRepository(jdbcTemplate, rowMapper);
    }

    @Test
    void testCreateTag() {
        String expectedSql = "SELECT tag_id, tag_name FROM tags WHERE tag_id = ? ";
        Tag tag = Tag.builder().build();
        int id = 1;
        when(jdbcTemplate.queryForObject(expectedSql, new Object[]{id}, rowMapper)).thenReturn(tag);

        Optional<Tag> actual = subject.find(id);

        verify(jdbcTemplate).queryForObject(expectedSql, new Object[]{id}, rowMapper);
        verifyNoMoreInteractions(jdbcTemplate);

        assertThat(actual).contains(tag);
    }

    @Test
    void testListTag() {
        String expectedSql = "SELECT tag_id, tag_name FROM tags";
        List<Tag> expected = new ArrayList<>(){};

        when(jdbcTemplate.query(expectedSql, rowMapper)).thenReturn(expected);

        List<Tag> actual = subject.findAll();

        verify(jdbcTemplate).query(expectedSql, rowMapper);
        verifyNoMoreInteractions(jdbcTemplate);

        assertThat(actual).isEqualTo(expected);
    }

    @Test
    void testGetTag() {
        String expectedSql = "SELECT tag_id, tag_name FROM tags WHERE tag_id = ? ";
        Tag tag = Tag.builder().build();
        int id = 1;

        when(jdbcTemplate.queryForObject(expectedSql, new Object[]{id}, rowMapper)).thenReturn(tag);

        Optional<Tag> actual = subject.find(id);

        verify(jdbcTemplate).queryForObject(expectedSql, new Object[]{id}, rowMapper);
        verifyNoMoreInteractions(jdbcTemplate);

        assertThat(actual).contains(tag);
    }

    @Test
    void testDeleteTag() {
        String expectedSql = "DELETE FROM public.tags WHERE tag_id = ?";

        subject.delete(1);

        verify(jdbcTemplate).update(expectedSql, 1);
        verifyNoMoreInteractions(jdbcTemplate);
    }
}

package org.example.service;

import org.example.model.Tag;
import org.example.repository.TagRepository;
import org.example.repository.mapper.TagRowMapper;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.junit.jupiter.MockitoExtension;

import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

import static org.assertj.core.api.Assertions.assertThat;
import static org.junit.jupiter.api.Assertions.assertTrue;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;

@ExtendWith(MockitoExtension.class)
public class TagServiceTest {

    @Mock
    private TagRepository repository;
    @Mock
    private TagRowMapper rowMapper;

    @InjectMocks
    private TagService subject;

    @Test
    void listTest() {
        List<Tag> tags = new ArrayList<>();
        when(repository.findAll()).thenReturn(tags);

        List<Tag> actual = subject.list();
        verify(repository).findAll();
        assertThat(actual).isEqualTo(tags);
    }

    @Test
    void createTest() {
        Tag tag = Tag.builder().build();
        subject.create(tag);
        verify(repository).add(tag);
    }

    @Test
    void getTest() {
        Tag tag = Tag.builder().build();
        subject.get(5);
        //when(repository.find(5)).thenReturn(Optional.ofNullable(tag));
        verify(repository).find(5);
    }

    @Test
    void deleteTest() {
        Tag tag = Tag.builder().build();
        subject.delete(7);

        verify(repository).delete(7);
    }

    @Test
    void existsNameTest_Found() throws SQLException {
        when(repository.existsByName("webdev")).thenReturn(true);
        boolean exists = subject.existsByName("webdev");
        verify(repository).existsByName("webdev");

        assertTrue(exists);
    }
}

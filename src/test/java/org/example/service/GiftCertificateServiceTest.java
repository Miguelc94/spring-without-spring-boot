package org.example.service;

import org.example.model.GiftCertificate;
import org.example.model.Tag;
import org.example.model.request.CreateGiftCertificateRequest;
import org.example.repository.GiftCertificateRepository;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.junit.jupiter.MockitoExtension;

import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

import static org.assertj.core.api.Assertions.assertThat;
import static org.junit.jupiter.api.Assertions.*;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.Mockito.*;

@ExtendWith(MockitoExtension.class)
class GiftCertificateServiceTest {

    @Mock
    private GiftCertificateRepository repository;
    @Mock
    private TagService tagService;

    @InjectMocks
    private GiftCertificateService subject;



    @Test
    void listTest() throws SQLException {
        List<GiftCertificate> certificates = new ArrayList<>();
        String[] params = {};

        when(repository.findAll()).thenReturn(certificates);

        List<GiftCertificate> actual = subject.list(params);

        verify(repository).findAll();
        verifyNoMoreInteractions(repository);
        assertThat(actual).isEqualTo(certificates);
    }

    @Test
    void createTest() {
        Tag firstTag = Tag.builder().build();
        Tag secondTag = Tag.builder().build();
        GiftCertificate giftCertificate = GiftCertificate.builder().build();

        CreateGiftCertificateRequest createcert = CreateGiftCertificateRequest.builder()
                .tags(List.of(firstTag, secondTag))
                .giftCertificate(giftCertificate)
                .build();

        subject.create(createcert);

        verify(tagService, times(2)).create(any(Tag.class));
        verify(repository).add(giftCertificate);
    }

    @Test
    void deleteTest() {
        GiftCertificate giftCertificate = GiftCertificate.builder().build();
        int id = giftCertificate.getGiftId();
        subject.delete(id);

        verify(repository).delete(id);
    }

    @Test
    void getTest() {
        subject.get(1);
        verify(repository).find(1);
    }

    @Test
    void updateTest() {
        GiftCertificate giftCertificate = GiftCertificate.builder().build();
        CreateGiftCertificateRequest createGift = CreateGiftCertificateRequest.builder().giftCertificate(giftCertificate).build();
        when(repository.existsById(2)).thenReturn(true);
        subject.update(createGift,2);

        verify(repository).update(giftCertificate,2);
        verify(repository).existsById(2);
    }

    @Test
    void existsByIdTest_Found() {
        when(repository.existsById(1)).thenReturn(true);

        boolean exists = subject.existsById(1);

        verify(repository).existsById(1);

        assertTrue(exists);
    }

    @Test
    void existsByIdTest_NotFound() {
        when(repository.existsById(99)).thenReturn(false);

        boolean exists = subject.existsById(99);

        verify(repository).existsById(99);

        assertFalse(exists);
    }
}
package org.example.repository.mapper;

import org.example.model.GiftCertificate;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.Mock;
import org.mockito.junit.jupiter.MockitoExtension;

import java.sql.ResultSet;
import java.sql.SQLException;

import static org.assertj.core.api.Assertions.assertThat;
import static org.junit.jupiter.api.Assertions.*;
import static org.mockito.Mockito.when;

@ExtendWith(MockitoExtension.class)
class GiftCertificateRowMapperTest {
    @Mock
    private ResultSet resultSet;
    private final GiftCertificateRowMapper subject = new GiftCertificateRowMapper();

    @Test
    void mapRow() throws SQLException {
        when(resultSet.getInt("gift_id")).thenReturn(1);
        when(resultSet.getString("gift_name")).thenReturn("name");
        when(resultSet.getString("description")).thenReturn("description");
        when(resultSet.getFloat("price")).thenReturn(1f);
        when(resultSet.getLong("duration")).thenReturn(1L);
        when(resultSet.getString("create_date")).thenReturn("creation");
        when(resultSet.getString("last_update_date")).thenReturn("update");

        GiftCertificate gift = subject.mapRow(resultSet,0);

        assertThat(gift).isNotNull();
        assertThat(gift.getGiftId()).isEqualTo(1);
        assertThat(gift.getGiftName()).isEqualTo("name");
        assertThat(gift.getDescription()).isEqualTo("description");
        assertThat(gift.getPrice()).isEqualTo(1f);
        assertThat(gift.getDuration()).isEqualTo(1L);
        assertThat(gift.getCreateDate()).isEqualTo("creation");
        assertThat(gift.getLastUpdateDate()).isEqualTo("update");
    }
}
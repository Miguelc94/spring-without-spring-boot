package org.example.repository.mapper;

import org.example.model.Tag;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.Mock;
import org.mockito.junit.jupiter.MockitoExtension;

import java.sql.ResultSet;
import java.sql.SQLException;

import static org.assertj.core.api.Assertions.assertThat;
import static org.junit.jupiter.api.Assertions.*;
import static org.mockito.Mockito.when;

@ExtendWith(MockitoExtension.class)
class TagRowMapperTest {

    @Mock
    private ResultSet resultSet;
    private final TagRowMapper subject = new TagRowMapper();

    @Test
    void mapRow() throws SQLException {
        when(resultSet.getInt("tag_id")).thenReturn(1);
        when(resultSet.getString("tag_name")).thenReturn("tag_name");

        Tag tag = subject.mapRow(resultSet, 0);

        assertThat(tag).isNotNull();
        assertThat(tag.getTagId()).isEqualTo(1);
        assertThat(tag.getTagName()).isEqualTo("tag_name");
    }
}